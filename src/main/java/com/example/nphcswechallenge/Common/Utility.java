package com.example.nphcswechallenge.Common;

public class Utility {
    public static Boolean isValidLength(String s, int minLength, int maxLength) {
        return s != null && (s.length() >= minLength && s.length() <= maxLength);
    }

    public static String append(String s1, String s2, String combinator) {
        String result = "";

        if (s1 != null)
            result += s1;

        if (s2 != null && s2.length() > 0) {
            if (combinator != null && result.length() > 0)
                result += combinator;

            result += s2;
        }

        return result;
    }

    public static String append(String s1, String s2) {
        String defaultCombinator = " ";
        return append(s1, s2, defaultCombinator);
    }

    public static boolean isValidDecimal(String value, int precision, int scale) {
        try {
            Double.parseDouble(value);
        } catch (Exception ex) {
            return false;
        }

        if (precision >= scale && value != null) {
            int lengthAllowedBeforeDP = precision - scale;

            String[] valueArray = value.split("\\.");
            if (valueArray[0].charAt(0) == '-')
                valueArray[0] = valueArray[0].substring(1);

            return ((valueArray[0].length() <= lengthAllowedBeforeDP
                    || (lengthAllowedBeforeDP == 0 && valueArray[0] == "0"))
                    && (valueArray.length == 1 || valueArray[1].length() <= scale));
        }

        return false;
    }

    public static boolean isNullOrEmpty(String value) {
        return (value == null || value.trim().length() == 0);
    }
}