package com.example.nphcswechallenge.Common;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class CSVReader {
    private static String separator = ",";

    public static List<String[]> parse(BufferedReader reader) throws IOException{
        List<String[]> result = new ArrayList<String[]>();
        
        String line;
        try {
            while((line = reader.readLine()) != null){
                result.add(line.split(separator));
            }
        } catch (IOException e) {
            throw e;
        }

        return result;
    }
}
