package com.example.nphcswechallenge.User;

import java.util.List;

public class UserResultsResponse {
    private final List<UserDataResponse> results;

    public UserResultsResponse(List<UserDataResponse> results) {
        this.results = results;
    }

    public List<UserDataResponse> getResults() {
        return results;
    }
}