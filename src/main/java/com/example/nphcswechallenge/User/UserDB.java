package com.example.nphcswechallenge.User;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.example.nphcswechallenge.Common.Utility;

import org.springframework.jdbc.core.JdbcTemplate;

public class UserDB {
    final String tableName = "T_USER";
    final String quotedTableName = "`" + tableName + "`";

    private JdbcTemplate jdbc;

    public UserDB(JdbcTemplate jdbc) {
        this.jdbc = jdbc;
    }

    public List<UserEntity> select(JdbcTemplate jdbc) throws SQLException {
        String sql = "SELECT * FROM " + quotedTableName;

        return jdbc.query(sql, (rs, rowNum) -> new UserEntity(rs.getString("id"), rs.getString("login"),
                rs.getString("name"), rs.getDouble("salary"), rs.getDate("startDate")));
    }

    public List<UserEntity> selectById(String id) throws SQLException {
        if (id == null)
            return null;

        String sql = "";
        sql = Utility.append(sql, "SELECT * FROM " + quotedTableName);
        sql = Utility.append(sql, "WHERE id = ?");

        return jdbc.query(sql, (rs, rowNum) -> new UserEntity(rs.getString("id"), rs.getString("login"),
                rs.getString("name"), rs.getDouble("salary"), rs.getDate("startDate")), id);
    }

    public int selectCountByID(String id) throws SQLException {
        return jdbc.queryForObject("SELECT COUNT(1) FROM " + quotedTableName + " WHERE id = ?", Integer.class, id);
    }

    public int selectCountByLoginButDiffId(String login, String id) throws SQLException {
        return jdbc.queryForObject("SELECT COUNT(1) FROM " + quotedTableName + " WHERE login = ? AND id <> ?",
                Integer.class, login, id);
    }

    public List<UserEntity> fetchQuery(Double minSalary, Double maxSalary, int offset, int limit, String filter,
            String sort) throws SQLException {
        List<Object> params = new ArrayList<Object>();

        String sql = "";
        sql = Utility.append(sql, "SELECT * FROM " + quotedTableName);

        // #region Condition
        String condition = "";
        condition = Utility.append(condition, "salary >= ?", " AND ");
        params.add(minSalary);

        condition = Utility.append(condition, "salary < ?", " AND ");
        params.add(maxSalary);

        if (filter.length() > 0) {
            filter = filter.toUpperCase();

            condition = Utility.append(condition, "(UPPER(id) LIKE ? OR UPPER(login) LIKE ? OR UPPER(name) LIKE ?)",
                    " AND ");
            params.add("%" + filter.replace(" ", "%") + "%");
            params.add("%" + filter.replace(" ", "%") + "%");
            params.add("%" + filter.replace(" ", "%") + "%");
        }

        sql = Utility.append(sql, "WHERE " + condition);
        // #endregion

        // #region Sort
        sql = Utility.append(sql, "ORDER BY " + sort);
        // #endregion

        // #region Limit
        if (limit > 0) {
            sql = Utility.append(sql, "LIMIT ?");
            params.add(limit);
        }
        // #endregion

        // #region Offset
        sql = Utility.append(sql, "OFFSET ?");
        params.add(offset);
        // #endregion

        return jdbc.query(sql, (rs, rowNum) -> new UserEntity(rs.getString("id"), rs.getString("login"),
                rs.getString("name"), rs.getDouble("salary"), rs.getDate("startDate")), params.toArray());
    }

    public void insert(UserEntity valid) throws SQLException {
        jdbc.update("INSERT INTO " + quotedTableName + " VALUES (?, ?, ?, ?, ?)", valid.id, valid.login, valid.name,
                valid.salary, valid.startDate);
    }

    public void update(UserEntity valid) throws SQLException {
        jdbc.update("UPDATE " + quotedTableName + " SET login = ?, name = ?, salary = ?, startDate = ? WHERE id = ?",
                valid.login, valid.name, valid.salary, valid.startDate, valid.id);
    }

    public void delete(String id) throws SQLException {
        jdbc.update("DELETE FROM " + quotedTableName + " WHERE id = ?", id);
    }
}