package com.example.nphcswechallenge.User;

import java.util.List;

import com.example.nphcswechallenge.Common.ObjMsgPair;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

@RestController
@ResponseBody
public class UserController {
	@Autowired
	private JdbcTemplate jdbc;

	@Autowired
	private TransactionTemplate transactionTemplate;

	private UserProcessing process;

	private void initProcess() {
		if (process == null) {
			process = new UserProcessing(transactionTemplate, new UserDB(jdbc));
		}
	}

	@PostMapping("/users/upload")
	public ResponseEntity<?> upload(@RequestParam(required = false) MultipartFile file) {
		initProcess();

		ObjMsgPair<Integer> result = process.processUpload(file);

		if (result.msg.length() == 0) {
			return new ResponseEntity<>(
					new UserMessageResponse("Upload successfully. " + result.obj + " record(s) is loaded."),
					result.obj == 0 ? HttpStatus.OK : HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(new UserMessageResponse(result.msg), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/users")
	public ResponseEntity<?> fetch(@RequestParam(required = false) String minSalary,
			@RequestParam(required = false) String maxSalary, @RequestParam(required = false) String offset,
			@RequestParam(required = false) String limit, @RequestParam(required = false) String filter,
			@RequestParam(required = false) String sort) {
		initProcess();

		ObjMsgPair<List<UserDataResponse>> result = process.processFetch(minSalary, maxSalary, offset, limit, filter,
				sort);

		if (result.msg.length() == 0) {
			return new ResponseEntity<>(new UserResultsResponse(result.obj), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new UserMessageResponse(result.msg), HttpStatus.BAD_REQUEST);
		}
	}

	@GetMapping("/users/{id}")
	public ResponseEntity<?> get(@PathVariable String id) {
		initProcess();

		ObjMsgPair<UserDataResponse> result = process.processGet(id);

		if (result.msg.length() == 0) {
			return new ResponseEntity<>(result.obj, HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new UserMessageResponse(result.msg), HttpStatus.BAD_REQUEST);
		}
	}

	@PostMapping("/users")
	public ResponseEntity<?> create(@RequestBody UserRaw raw) {
		initProcess();

		String result = process.processCreate(raw);

		if (result.length() == 0) {
			return new ResponseEntity<>(new UserMessageResponse("Successfully created"), HttpStatus.CREATED);
		} else {
			return new ResponseEntity<>(new UserMessageResponse(result), HttpStatus.BAD_REQUEST);
		}
	}

	@PutMapping("/users/{id}")
	public ResponseEntity<?> updatePut(@RequestBody UserRaw raw, @PathVariable String id) {
		return update(raw, id);
	}

	@PatchMapping("/users/{id}")
	public ResponseEntity<?> updatePatch(@RequestBody UserRaw raw, @PathVariable String id) {
		return update(raw, id);
	}

	private ResponseEntity<?> update(UserRaw raw, String id) {
		initProcess();

		String result = process.processUpdate(raw, id);

		if (result.length() == 0) {
			return new ResponseEntity<>(new UserMessageResponse("Successfully updated"), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new UserMessageResponse(result), HttpStatus.BAD_REQUEST);
		}
	}

	@DeleteMapping("/users/{id}")
	public ResponseEntity<?> remove(@PathVariable String id) {
		initProcess();

		String result = process.processRemove(id);

		if (result.length() == 0) {
			return new ResponseEntity<>(new UserMessageResponse("Successfully deleted"), HttpStatus.OK);
		} else {
			return new ResponseEntity<>(new UserMessageResponse(result), HttpStatus.BAD_REQUEST);
		}
	}
}