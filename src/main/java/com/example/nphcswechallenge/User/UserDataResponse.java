package com.example.nphcswechallenge.User;

import java.util.Date;

public class UserDataResponse {
    public String id;
    public String name;
    public String login;
    public Double salary;
    public Date startDate;

    public UserDataResponse() {
    }

    public UserDataResponse(String id, String name, String login, Double salary, Date startDate) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.salary = salary;
        this.startDate = startDate;
    }
}