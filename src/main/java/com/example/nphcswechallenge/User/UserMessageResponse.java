package com.example.nphcswechallenge.User;

public class UserMessageResponse {
    private final String message;

    public UserMessageResponse(String message){
        this.message = message;
    }

    public String getMessage(){
        return message;
    }
}