package com.example.nphcswechallenge.User;

public class UserRaw {
    public int rowNo;
    
    public String id;
    public String login;
    public String name;
    public String salary;
    public String startDate;

    public UserRaw() {
    }

    public UserRaw(String[] values, int rowNo) {
        if(values.length == 5){
            this.rowNo = rowNo;
            
            id = values[0];
            login = values[1];
            name = values[2];
            salary = values[3];
            startDate = values[4];
        }
    }
}