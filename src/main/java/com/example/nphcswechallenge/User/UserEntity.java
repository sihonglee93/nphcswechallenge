package com.example.nphcswechallenge.User;

import java.util.Date;

public class UserEntity {
    public String id;
    public String login;
    public String name;
    public Double salary;
    public Date startDate;

    public int rowNo;

    public UserEntity() {

    }

    public UserEntity(String id, String login, String name, Double salary, Date startDate) {
        this.id = id;
        this.login = login;
        this.name = name;
        this.salary = salary;
        this.startDate = startDate;
    }
}