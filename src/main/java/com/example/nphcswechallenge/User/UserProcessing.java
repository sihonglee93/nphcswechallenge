package com.example.nphcswechallenge.User;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.sql.SQLException;
import java.text.SimpleDateFormat;
import com.example.nphcswechallenge.Common.Utility;
import com.example.nphcswechallenge.Common.CSVReader;
import com.example.nphcswechallenge.Common.ObjMsgPair;
import org.springframework.transaction.support.TransactionTemplate;
import org.springframework.web.multipart.MultipartFile;

public class UserProcessing {
    private TransactionTemplate transactionTemplate;
    private UserDB db;

    public UserProcessing(TransactionTemplate transactionTemplate, UserDB db) {
        this.transactionTemplate = transactionTemplate;
        this.db = db;
    }

    public ObjMsgPair<Integer> processUpload(MultipartFile file) {
        ObjMsgPair<Integer> result = new ObjMsgPair<Integer>();

        if (file != null) {
            ObjMsgPair<List<UserRaw>> parseResult = parseCSV(file);
            if (parseResult.msg.length() == 0) {
                List<UserRaw> raws = parseResult.obj;

                ObjMsgPair<List<UserEntity>> validateResult = validateBatch(raws);

                if (validateResult.msg.length() == 0) {
                    return saveBatch(validateResult.obj);
                } else {
                    result.msg = validateResult.msg;
                    return result;
                }
            } else {
                result.msg = parseResult.msg;
                return result;
            }
        } else {
            result.msg = "No file is uploaded";
            return result;
        }
    }

    private ObjMsgPair<List<UserRaw>> parseCSV(MultipartFile file) {
        ObjMsgPair<List<UserRaw>> result = new ObjMsgPair<List<UserRaw>>();
        result.obj = new ArrayList<UserRaw>();

        try (BufferedReader reader = new BufferedReader(new InputStreamReader(file.getInputStream()))) {
            List<String[]> parsedResult = CSVReader.parse(reader);

            int rowLoaded = 0;
            int rowNo = 0;
            for (String[] data : parsedResult) {
                rowNo++;

                if (data.length > 0) {
                    if ((data.length == 1 && data[0].length() == 0)
                            || (data[0].length() > 0 && data[0].charAt(0) == '#')) { // Skip empty &
                        // comment line
                        continue;
                    }

                    if (rowLoaded == 0) { // Header
                        if (!validateHeader(data)) {
                            result.msg = Utility.append(result.msg, "Header is invalid.");
                            break;
                        }
                    } else { // Content
                        if (data.length == 5) {
                            result.obj.add(new UserRaw(data, rowNo));
                        } else {
                            result.msg = Utility.append(result.msg, "Incorrect field count at Row #" + rowNo + ".");
                        }
                    }

                    rowLoaded++;
                }
            }
        } catch (Exception ex) {
            result.msg = Utility.append(result.msg, "File is failed to read.");
        }

        return result;
    }

    private Boolean validateHeader(String[] headers) {
        if (headers.length != 5 || !headers[0].equals("id") || !headers[1].equals("login") || !headers[2].equals("name")
                || !headers[3].equals("salary") || !headers[4].equals("startDate")) {
            return false;
        }

        return true;
    }

    private ObjMsgPair<List<UserEntity>> validateBatch(List<UserRaw> raws) {
        ObjMsgPair<List<UserEntity>> result = new ObjMsgPair<List<UserEntity>>();

        result.obj = new ArrayList<UserEntity>();

        // #region Validate duplicate [id] in file
        ArrayList<String> idList = new ArrayList<String>();
        HashSet<String> idHashSet = new HashSet<String>();
        for (UserRaw raw : raws) {
            idList.add(raw.id);
            idHashSet.add(raw.id);
        }

        if (idList.size() != idHashSet.size()) {
            result.msg = Utility.append(result.msg, "More than one row with same employee ID.");
        }
        // #endregion

        for (UserRaw raw : raws) {
            ObjMsgPair<UserEntity> validateResult = validate(raw);

            if (validateResult.msg.length() == 0) {
                result.obj.add(validateResult.obj);
            } else {
                result.msg = Utility.append(result.msg, "Row #" + raw.rowNo + ": " + validateResult.msg + ".");
            }
        }

        return result;
    }

    private ObjMsgPair<UserEntity> validate(UserRaw raw) {
        ObjMsgPair<UserEntity> result = new ObjMsgPair<UserEntity>();

        result.obj = new UserEntity();
        result.obj.rowNo = raw.rowNo;

        // #region Validate id
        if (Utility.isValidLength(raw.id, 1, 10)) {
            result.obj.id = raw.id;
        } else {
            result.msg = Utility.append(result.msg, "Invalid employee ID");
            return result;
        }
        // #endregion

        // #region Validate login
        if (Utility.isValidLength(raw.login, 1, 20)) {
            result.obj.login = raw.login;
        } else {
            result.msg = Utility.append(result.msg, "Invalid employee login");
            return result;
        }
        // #endregion

        // #region Validate name
        if (Utility.isValidLength(raw.name, 1, 100)) {
            result.obj.name = raw.name;
        } else {
            result.msg = Utility.append(result.msg, "Invalid name");
            return result;
        }
        // #endregion

        // #region Validate salary
        if (Utility.isValidDecimal(raw.salary, 12, 2)) {
            result.obj.salary = Double.parseDouble(raw.salary);

            if (result.obj.salary < 0) {
                result.msg = Utility.append(result.msg, "Invalid salary");
                return result;
            }
        } else {
            result.msg = Utility.append(result.msg, "Invalid salary");
            return result;
        }
        // #endregion

        // #region Validate startDate
        if (Utility.isValidLength(raw.startDate, 9, 10)) {
            SimpleDateFormat sdf = null;

            if (raw.startDate.length() == 10) {
                sdf = new SimpleDateFormat("yyyy-MM-dd");
            } else if (raw.startDate.length() == 9) {
                sdf = new SimpleDateFormat("dd-MMM-yy");
            }

            try {
                result.obj.startDate = sdf.parse(raw.startDate);
            } catch (Exception ex) {
                result.msg = Utility.append(result.msg, "Invalid date");
                return result;
            }
        } else {
            result.msg = Utility.append(result.msg, "Invalid date");
            return result;
        }
        // #endregion

        return result;
    }

    private ObjMsgPair<Integer> saveBatch(List<UserEntity> valids) {
        ObjMsgPair<Integer> result = new ObjMsgPair<Integer>();
        result.obj = 0;

        return transactionTemplate.execute(status -> {
            for (UserEntity valid : valids) {
                try {
                    // #region Check for same [login]
                    if (db.selectCountByLoginButDiffId(valid.login, valid.id) > 0) {
                        result.msg = "Row #" + valid.rowNo + ": Employee login not unique";
                        status.setRollbackOnly();
                        return result;
                    }
                    // #endregion

                    // #region Insert / Update
                    if (db.selectCountByID(valid.id) == 0) {
                        db.insert(valid);
                    } else {
                        db.update(valid);
                    }
                    // #endregion
                } catch (Exception ex) {
                    result.msg = "Record save failed";
                    status.setRollbackOnly();
                    return result;
                }

                result.obj++;
            }

            return result;
        });
    }

    public ObjMsgPair<List<UserDataResponse>> processFetch(String minSalaryParam, String maxSalaryParam,
            String offsetParam, String limitParam, String filterParam, String sortParam) {
        ObjMsgPair<List<UserDataResponse>> result = new ObjMsgPair<List<UserDataResponse>>();

        Double minSalary;
        Double maxSalary;
        int offset;
        int limit;

        // #region Parse params
        try {
            if (Utility.isNullOrEmpty(minSalaryParam))
                minSalaryParam = "0";

            minSalary = Double.parseDouble(minSalaryParam);
        } catch (Exception ex) {
            result.msg = "Invalid minSalary";
            return result;
        }

        try {
            if (Utility.isNullOrEmpty(maxSalaryParam))
                maxSalaryParam = "4000";

            maxSalary = Double.parseDouble(maxSalaryParam);
        } catch (Exception ex) {
            result.msg = "Invalid maxSalary";
            return result;
        }

        try {
            if (Utility.isNullOrEmpty(offsetParam))
                offsetParam = "0";

            offset = Integer.parseInt(offsetParam);
        } catch (Exception ex) {
            result.msg = "Invalid offset";
            return result;
        }

        try {
            if (Utility.isNullOrEmpty(limitParam))
                limitParam = "0";

            limit = Integer.parseInt(limitParam);
        } catch (Exception ex) {
            result.msg = "Invalid limit";
            return result;
        }

        String filter = !Utility.isNullOrEmpty(filterParam) ? filterParam : "";

        String sort = "id";
        if (!Utility.isNullOrEmpty(sortParam)) {
            List<String> availableSort = new ArrayList<>(
                    Arrays.asList(new String[] { "ID", "LOGIN", "NAME", "SALARY", "STARTDATE" }));
            if (availableSort.contains(sortParam.toUpperCase())) {
                sort = sortParam;
            } else {
                result.msg = "Invalid sort";
                return result;
            }
        }
        // #endregion

        List<UserEntity> records;
        try {
            records = db.fetchQuery(minSalary, maxSalary, offset, limit, filter, sort);
        } catch (SQLException e) {
            result.msg = "Failed to fetch";
            records = new ArrayList<UserEntity>();
        }

        result.obj = new ArrayList<UserDataResponse>();
        for (UserEntity record : records) {
            result.obj.add(new UserDataResponse(record.id, record.name, record.login, record.salary, record.startDate));
        }

        return result;
    }

    public ObjMsgPair<UserDataResponse> processGet(String id) {
        ObjMsgPair<UserDataResponse> result = new ObjMsgPair<UserDataResponse>();

        List<UserEntity> records;
        try {
            records = db.selectById(id);
        } catch (SQLException e) {
            result.msg = "Failed to fetch";
            records = new ArrayList<UserEntity>();
        }

        if (records.size() > 0) {
            UserEntity record = records.get(0);
            result.obj = new UserDataResponse(record.id, record.name, record.login, record.salary, record.startDate);
        } else {
            result.msg = Utility.append(result.msg, "No such employee");
        }

        return result;
    }

    public String processCreate(UserRaw raw) {
        ObjMsgPair<UserEntity> validateResult = validate(raw);

        if (validateResult.msg.length() == 0) {
            UserEntity valid = validateResult.obj;

            try {
                // #region Check if exists
                if (db.selectCountByID(valid.id) == 1) {
                    return "Employee ID already exists";
                }
                // #endregion

                // #region Check login unique
                if (db.selectCountByLoginButDiffId(valid.login, valid.id) > 0) {
                    return "Employee login not unique";
                }
                // #endregion
            } catch (Exception ex) {
                return "Failed to query database";
            }

            try {
                db.insert(valid);
            } catch (Exception ex) {
                return "Create failed";
            }
        } else {
            return validateResult.msg;
        }

        return "";
    }

    public String processUpdate(UserRaw raw, String id) {
        raw.id = id;

        ObjMsgPair<UserEntity> validateResult = validate(raw);
        if (validateResult.msg.length() == 0) {
            UserEntity valid = validateResult.obj;

            try {
                // #region Check if exists
                if (db.selectCountByID(valid.id) == 0) {
                    return "No such employee";
                }
                // #endregion

                // #region Check login unique
                if (db.selectCountByLoginButDiffId(valid.login, valid.id) > 0) {
                    return "Employee login not unique";
                }
                // #endregion
            } catch (Exception ex) {
                return "Failed to query database";
            }

            try {
                db.update(valid);
            } catch (Exception ex) {
                return "Update failed";
            }
        } else {
            return validateResult.msg;
        }

        return "";
    }

    public String processRemove(String id) {
        UserEntity valid = new UserEntity();
        valid.id = id;

        try {
            // #region Check if exists
            if (db.selectCountByID(valid.id) == 0) {
                return "No such employee";
            }
            // #endregion
        } catch (Exception ex) {
            return "Failed to query database";
        }

        try {
            db.delete(valid.id);
        } catch (Exception ex) {
            return "Delete failed";
        }

        return "";
    }
}