package com.example.nphcswechallenge;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NphcswechallengeApplication {
	public static void main(String[] args) {
		SpringApplication.run(NphcswechallengeApplication.class, args);
	}
}