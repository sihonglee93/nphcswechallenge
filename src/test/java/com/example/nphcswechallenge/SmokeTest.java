package com.example.nphcswechallenge;

import static org.assertj.core.api.Assertions.assertThat;
import com.example.nphcswechallenge.User.UserController;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.MethodMode;

@SpringBootTest
class SmokeTest {
	@Autowired
	private UserController controller;

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void contextLoads() throws Exception {
		assertThat(controller).isNotNull();
	}
}