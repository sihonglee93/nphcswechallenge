package com.example.nphcswechallenge;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileInputStream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class FetchTest {
	@Autowired
	private MockMvc mockMvc;

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void badParam() throws Exception {
		this.mockMvc.perform(get("/users").param("minSalary", "abc")).andDo(print()).andExpect(status().isBadRequest())
				.andExpect(content().string(containsString("{\"message\":\"Invalid minSalary\"}")));
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void zeroRecord() throws Exception {
		this.mockMvc.perform(get("/users")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString("{\"results\":[]}")));
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void noParam() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/Sample.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file));

		this.mockMvc.perform(get("/users")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString(
						"{\"results\":[{\"id\":\"e0001\",\"name\":\"Harry Potter\",\"login\":\"hpotter\",\"salary\":1234.0,\"startDate\":\"2001-11-16\"}]}")));
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void hasParam() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/Sample.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file));

		this.mockMvc.perform(get("/users").param("maxSalary", "19234.51")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString(
						"{\"results\":[{\"id\":\"e0001\",\"name\":\"Harry Potter\",\"login\":\"hpotter\",\"salary\":1234.0,\"startDate\":\"2001-11-16\"},{\"id\":\"e0002\",\"name\":\"Ron Weasley\",\"login\":\"rwesley\",\"salary\":19234.5,\"startDate\":\"2001-11-16\"}]}")));
	}
}