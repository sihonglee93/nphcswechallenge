package com.example.nphcswechallenge;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.patch;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileInputStream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class CRUDTest {
	@Autowired
	private MockMvc mockMvc;

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void getNoSuchEmployee() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/Sample.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file));

		this.mockMvc.perform(get("/users/e9999")).andDo(print()).andExpect(status().isBadRequest())
				.andExpect(content().string(containsString("{\"message\":\"No such employee\"}")));
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void getEmployee() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/Sample.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file));

		this.mockMvc.perform(get("/users/e0001")).andDo(print()).andExpect(status().isOk())
				.andExpect(content().string(containsString(
						"{\"id\":\"e0001\",\"name\":\"Harry Potter\",\"login\":\"hpotter\",\"salary\":1234.0,\"startDate\":\"2001-11-16\"}")));
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void createLoginNotUnique() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/Sample.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file));

		this.mockMvc.perform(post("/users").contentType(MediaType.APPLICATION_JSON).content(
				"{\"id\": \"emp0001\",\"name\": \"Harry Potter\",\"login\": \"hpotter\",\"salary\": 1234.00,\"startDate\": \"2001-11-16\"}"))
				.andDo(print())
				.andExpect(content().string(containsString("{\"message\":\"Employee login not unique\"}")))
				.andExpect(status().isBadRequest());
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void createSuccessfully() throws Exception {
		this.mockMvc.perform(post("/users").contentType(MediaType.APPLICATION_JSON).content(
				"{\"id\": \"e0001\",\"name\": \"Harry Potter\",\"login\": \"hpotter\",\"salary\": 1234.0,\"startDate\": \"16-Nov-01\"}"))
				.andDo(print()).andExpect(content().string(containsString("{\"message\":\"Successfully created\"}")))
				.andExpect(status().isCreated());
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void updatePutNoSuchEmployee() throws Exception {
		this.mockMvc.perform(put("/users/e0001").contentType(MediaType.APPLICATION_JSON).content(
				"{\"name\": \"Harry Potter\",\"login\": \"hpotter\",\"salary\": 1234.0,\"startDate\": \"16-Nov-01\"}"))
				.andDo(print()).andExpect(content().string(containsString("{\"message\":\"No such employee\"}")))
				.andExpect(status().isBadRequest());
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void updatePutSuccessfully() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/Sample.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file));

		this.mockMvc.perform(put("/users/e0001").contentType(MediaType.APPLICATION_JSON).content(
				"{\"name\": \"Harry Potter\",\"login\": \"hpotter\",\"salary\": 1234.0,\"startDate\": \"16-Nov-01\"}"))
				.andDo(print()).andExpect(content().string(containsString("{\"message\":\"Successfully updated\"}")))
				.andExpect(status().isOk());
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void updatePatchNoSuchEmployee() throws Exception {
		this.mockMvc.perform(patch("/users/e0001").contentType(MediaType.APPLICATION_JSON).content(
				"{\"name\": \"Harry Potter\",\"login\": \"hpotter\",\"salary\": 1234.0,\"startDate\": \"16-Nov-01\"}"))
				.andDo(print()).andExpect(content().string(containsString("{\"message\":\"No such employee\"}")))
				.andExpect(status().isBadRequest());
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void updatePatchSuccessfully() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/Sample.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file));

		this.mockMvc.perform(patch("/users/e0001").contentType(MediaType.APPLICATION_JSON).content(
				"{\"name\": \"Harry Potter\",\"login\": \"hpotter\",\"salary\": 1234.0,\"startDate\": \"16-Nov-01\"}"))
				.andDo(print()).andExpect(content().string(containsString("{\"message\":\"Successfully updated\"}")))
				.andExpect(status().isOk());
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void deleteNoSuchEmployee() throws Exception {
		this.mockMvc.perform(delete("/users/e0001")).andDo(print())
				.andExpect(content().string(containsString("{\"message\":\"No such employee\"}")))
				.andExpect(status().isBadRequest());
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void deleteSuccessfully() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/Sample.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file));

		this.mockMvc.perform(delete("/users/e0001")).andDo(print())
				.andExpect(content().string(containsString("{\"message\":\"Successfully deleted\"}")))
				.andExpect(status().isOk());
	}
}