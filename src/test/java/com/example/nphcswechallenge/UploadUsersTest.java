package com.example.nphcswechallenge;

import static org.hamcrest.Matchers.containsString;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.multipart;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.io.File;
import java.io.FileInputStream;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.mock.web.MockMultipartFile;
import org.springframework.test.annotation.DirtiesContext;
import org.springframework.test.annotation.DirtiesContext.MethodMode;
import org.springframework.test.web.servlet.MockMvc;

@SpringBootTest
@AutoConfigureMockMvc
class UploadUsersTest {
	@Autowired
	private MockMvc mockMvc;

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void noFileUploaded() throws Exception {
		this.mockMvc.perform(multipart("/users/upload")).andDo(print()).andExpect(status().isBadRequest())
				.andExpect(content().string(containsString("{\"message\":\"No file is uploaded\"}")));
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void emptyFileUploaded() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/Empty.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file)).andDo(print()).andExpect(status().isOk()).andExpect(
				content().string(containsString("{\"message\":\"Upload successfully. 0 record(s) is loaded.\"}")));
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void headerOnlyFileUploaded() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/HeaderOnly.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file)).andDo(print()).andExpect(status().isOk()).andExpect(
				content().string(containsString("{\"message\":\"Upload successfully. 0 record(s) is loaded.\"}")));
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void invalidHeaderFileUploaded() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/InvalidHeader.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file)).andDo(print()).andExpect(status().isBadRequest())
				.andExpect(content().string(containsString("{\"message\":\"Header is invalid.\"}")));
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void dataWithExtraColumn() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/DataWithExtraColumn.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file)).andDo(print()).andExpect(status().isBadRequest())
				.andExpect(content().string(containsString("{\"message\":\"Incorrect field count at Row #2.\"}")));
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void duplicateId() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/DuplicateId.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file)).andDo(print()).andExpect(status().isBadRequest())
				.andExpect(
						content().string(containsString("{\"message\":\"More than one row with same employee ID.\"}")));
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void invalidId() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/InvalidId.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file)).andDo(print()).andExpect(status().isBadRequest())
				.andExpect(content().string(
						containsString("{\"message\":\"Row #2: Invalid employee ID. Row #3: Invalid employee ID.\"}")));
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void invalidLogin() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/InvalidLogin.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file)).andDo(print()).andExpect(status().isBadRequest())
				.andExpect(content().string(containsString(
						"{\"message\":\"Row #2: Invalid employee login. Row #3: Invalid employee login.\"}")));
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void invalidName() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/InvalidName.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file)).andDo(print()).andExpect(status().isBadRequest())
				.andExpect(content()
						.string(containsString("{\"message\":\"Row #2: Invalid name. Row #3: Invalid name.\"}")));
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void invalidSalary() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/InvalidSalary.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file)).andDo(print()).andExpect(status().isBadRequest())
				.andExpect(content()
						.string(containsString("{\"message\":\"Row #2: Invalid salary. Row #3: Invalid salary.\"}")));
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void invalidStartDate() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/InvalidStartDate.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file)).andDo(print()).andExpect(status().isBadRequest())
				.andExpect(content()
						.string(containsString("{\"message\":\"Row #2: Invalid date. Row #3: Invalid date.\"}")));
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void LoginNotUnique() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/LoginNotUnique.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file)).andDo(print()).andExpect(status().isBadRequest())
				.andExpect(content()
						.string(containsString("{\"message\":\"Row #3: Employee login not unique\"}")));
	}

	@DirtiesContext(methodMode = MethodMode.BEFORE_METHOD)
	@Test
	public void sampleFileUploaded() throws Exception {
		MockMultipartFile file = new MockMultipartFile("file",
				new FileInputStream(new File("src/test/resources/UploadUsersTest/Sample.csv")));

		this.mockMvc.perform(multipart("/users/upload").file(file)).andDo(print()).andExpect(status().isCreated())
				.andExpect(content()
						.string(containsString("{\"message\":\"Upload successfully. 2 record(s) is loaded.\"}")));
	}
}