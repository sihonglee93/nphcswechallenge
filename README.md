# Technology #

Project is built with:

- Spring Boot 2.5.1
- Java 11
- H2 Database

# Prerequisites #

- JDK 11 or newer

# Installation #

1. Clone repo

        git clone https://sihonglee93@bitbucket.org/sihonglee93/nphcswechallenge.git

# Build #

1. Build project

        ..\nphcswechallenge\src\main\java\com\example\nphcswechallenge\NphcswechallengeApplication.java

# Unit Test #

- Unit Test Project

        ..\nphcswechallenge\src\test\java\com\example\nphcswechallenge\

# Test #

1. Run project

1. Test against http://localhost:8080

# Schema Design #

## User ##

Field|Data Type
-|-
id|VARCHAR(10)
login|VARCHAR(20)
name|NVARCHAR(200)
salary|DECIMAL(12,2)
startDate|DATE

# Bonus Feature #

## Fetch List of Employees ##
Request Param|Description
-|-
filter|Filter results with records that contains param in [id] / [login] / [name] fields only.
sort|Sort records using param. (E.g. id, login, name, salary, startDate)